!function (window, document) {
    try {
        with (document) {
            title=title.replace(/婺源人才网/ig, '婺源招聘网');
            // 取得版本
            var version = getElementsByTagName('html')[0].getAttribute('data-v');
            // 判断是否为招聘版
            if (version != 'job') {
                return;
            }
            var siteName = '婺源招聘网';
            var logoUrl  = CDN + 'ups/_/8e965d9859b28de0.jpeg';
            var pageId   = body.id;
            var url      = location.pathname;
            // 搜索栏上的 placeholder
            var q        = querySelector('*[name=q]');
            if (q) {
                q.setAttribute('placeholder', '找企业，找工作就上婺源招聘网');
                searchLogo()
            }
            // 搜索栏上的 LOGO
            function searchLogo() {
                with (document.querySelector('.sec-input')) {
                    if (tagName) {
                        style.backgroundImage    = 'url(' + logoUrl + ')';
                        style.backgroundRepeat   = 'no-repeat';
                        style.backgroundSize     = '32px 32px';
                        style.backgroundPosition = '8px 3px';
                        style.opacity            = 0.8;
                    }
                }
            }

            function WXconf(WXtitle) {
                if (WXCONF) {
                    var num       = /\（(.+?)\）/g.exec(WXCONF.title);
                    WXCONF.title  = (num == null) ? (siteName + WXtitle) : (siteName + WXtitle + num[0]);
                    WXCONF.imgUrl = logoUrl;
                    WXCONF.desc   = '婺源招聘网是新今日婺源打造的一家最专业的招聘求职平台，更专注！更高效！为用工单位招聘服务，为求职人员就业搭桥。';
                }
            }

            var header = querySelector('.header'), h2 = false;
            if (header) {
                h2 = header.querySelector('h2');
            }

            // 分享修改接口：
            // WXCONF ={imgUrl:"图标地址",desc:"描述文字",title:"分享标题"};
            switch (pageId) {
                case 'job-index'://首页
                    if (h2) {
                        h2.innerText = siteName;
                    }
                    // 招聘首页标题
                    title = siteName + '—首页';
                    WXconf('招聘求职首页');
                    break;
                case 'job-zhaopin'://招聘首页
                    title = siteName + '—招聘';
                    WXconf('招聘信息');
                    break;
                case 'job-qiuzhi'://求职首页
                    title = siteName + '—求职';
                    WXconf('求职信息');
                    break;
                case 'job-shop'://认证企业
                    querySelector('.header').querySelector('a').innerText = siteName;
                    title                                                 = siteName + '—认证企业';
                    WXconf('认证企业信息');
                    break;
                case 'job-pub_zhaopin'://招聘发布
                    title = '发布招聘';
                    WXconf('发布招聘');
                    break;
                case 'job-pub_qiuzhi'://求职发布
                    title = '发布求职';
                    WXconf('发布求职');
                    break;
                case 'group-search'://搜索
                    title = q.getAttribute('value') + '-' + siteName;
                    break;
                case 'group-edit'://发布
                    if (querySelector('.advantage-page')) {
                        title = '入驻企业—' + siteName;
                    } else {
                        title = h2.innerText + '—' + siteName;
                    }
                    break;
            }
        }
    } catch (e) {
        var img = new Image();
        //DIR +
        img.src = '/c.gif?_=js&m=' + e.message;
    }
}(window, document);